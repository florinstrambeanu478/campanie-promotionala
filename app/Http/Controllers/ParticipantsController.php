<?php

namespace App\Http\Controllers;

use App\Participant;
use App\Token;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ParticipantsController extends Controller
{
    public function create()
    {
        return view('enter-contest');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone_number' => 'required',
            'token' => 'required'
        ]);

        $token = Token::where('value', '=', $request->get('token'))->first();

        if ($token == null) {
            return back()->with('message', 'Cod promotional invalid!');
        }

        if ($token->used) {
            return back()->with('message', 'Codul promotional este deja folosit!');
        }
        $currentDate = new \DateTime();
        if ($currentDate < new \DateTime($token->start_date)) {
            return back()->with('message', 'Campania inca nu a inceput');
        }

        if ($currentDate > new \Datetime($token->end_date)) {
            return back()->with('message', 'Campania s-a terminat');
        }

        $participant = new Participant([
            'name' => $request->get('name'),
            'phone_number' => $request->get('phone_number'),
            'token' => $request->get('token')
        ]);

        if (!$participant->save()) {
            return back()->with('message', 'Could not save participant');
        }

        $token->used = true;

        $token->save();

        return back()->with('message', 'Congrats, token saved!');
    }
}
