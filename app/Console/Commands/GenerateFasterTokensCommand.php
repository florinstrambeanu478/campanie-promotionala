<?php

namespace App\Console\Commands;

use App\Participant;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class GenerateFasterTokensCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:fasterTokens {numberOfTokens} {startDate} {endDate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $startDate = new \DateTime($this->argument('startDate'));
        $endDate = new \DateTime($this->argument('endDate'));
        $numberOfTokens = $this->argument('numberOfTokens');
        $chunkSize = 1000;
        $quotient = intdiv($numberOfTokens, $chunkSize);
        $remainder = $numberOfTokens % $chunkSize;
        $steps = $quotient;

        for ($i = 0; $i <= $steps; $i++) {
            $promoCodes = [];

            if ($i == $steps) {
                $chunkSize = $remainder;
            }

            for ($j = 0; $j < $chunkSize; $j++) {
                $promoCodes[] = [
                    'value' => Str::random(8),
                    'start_date' => $startDate,
                    'end_date' => $endDate,
                ];

            }

            DB::table('tokens')->insert($promoCodes);
        }
    }
}
