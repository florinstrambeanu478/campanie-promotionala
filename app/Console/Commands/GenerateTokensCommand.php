<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class GenerateTokensCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:tokens {numberOfTokens} {startDate} {endDate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        $consoleOutput = new ConsoleOutput();

        $startDate = new \DateTime($this->argument('startDate'));
        $endDate = new \DateTime($this->argument('endDate'));
        $numberOfTokens = $this->argument('numberOfTokens');
        $chunkSize = 1000;
        $steps = ceil($numberOfTokens / $chunkSize);
        $inserted = 0;

        for ($i = 0; $i < $steps; $i++) {
            $promotionalCodes = [];

            for ($j = 0; $j < $chunkSize; $j++) {
                $inserted++;

                $promotionalCodes[] = [
                    'value' => Str::random(8),
                    'start_date' => $startDate,
                    'end_date' => $endDate,
                ];
            }
            $consoleOutput->writeln(sprintf('Inserted %s codes', $inserted));
            DB::table('tokens')->insert($promotionalCodes);
        }

    }
}
