<!doctype html>
<html lang="en">
<head>
    @include('layout.partials.head')
</head>

<body>



<main role="main" class="container">

    <div class="starter-template" style="margin-top: 100px;">
        <div class="row justify-content-center">
            <div class="col-sm-6">
                @yield('content')
            </div>
        </div>
    </div>

</main><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
@include('layout.partials.footer-scripts');
</body>
</html>
