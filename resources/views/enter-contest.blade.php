@extends('layout.layout')

@section('content')
    <h1>Introduceti datele</h1>
    <hr>
    <form action="{{route('store_participant')}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Nume</label>
            <input type="text" class="form-control" id="contestantName"  name="name">
        </div>

        <div class="form-group">
            <label for="description">Telefon</label>
            <input type="text" class="form-control" id="phoneNumber" name="phone_number"/>
        </div>

        <div class="form-group">
            <label for="description">Cod Promotional</label>
            <input type="text" class="form-control" id="promotionalNumber" name="token"/>
        </div>


        <div class="form-group">
            <label for="description">Am citit si sunt de acord cu politica de confidentialitate</label><br/>
            <input type="checkbox" id="rulesAgreement" name="agreement", required>
        </div>

        <div class="form-group">
            <label for="description">Am citit regulamentul si sunt de acord</label><br/>
            <input type="checkbox" id="rulesAgreement" name="agreement", required>
        </div>



        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    @if (session('message'))
        <div class="alert alert-danger">
        {{session('message')}}
        </div>
    @endif
@endsection
